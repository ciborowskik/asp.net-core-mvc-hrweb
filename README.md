# HrWebCiborowski

Jest to aplikacja webowa, która powstała jako projekt studencki. 
[https://hrwebciborowski.azurewebsites.net](https://hrwebciborowski.azurewebsites.net)

## Użyte technologie

W projekcie został użyty framework ASP.NET Core MVC. Projekt jest opublikowany na serwerach Microsoft Azure pod adresem [https://hrwebciborowski.azurewebsites.net](https://hrwebciborowski.azurewebsites.net). Do przechowywania danych użyto bazy danych Microsoft SQL Server oraz Azure Storage. Dostęp do bazy danych odbywa się za pośrednictwem Entity Framework. Aplikacja umożliwia rejestrację użytkowników. Ich dane są przechowywane w usłudze Azure Active Directory B2C. Aplikacja jest zsynchronizowana z usługą Share Point z pakietu Microsoft Office 365. Część operacji w aplikacji można wykonać za pośrednictwem API. Dokumentacja API została wygenerowana przy pomocy narzędzia Swagger i jest dostępna pod adresem [https://hrwebciborowski.azurewebsites.net/swagger](https://hrwebciborowski.azurewebsites.net/swagger).

## Funkcjonalność

Aplikacja ma symulować portal używany przez pracowników działów HR. Witryna umożliwia dodawanie ofert pracy oraz aplikowanie do nich poprzez wypełnienie danych i załączenie pliku z CV. Informacje o nowych aplikacjach są przesyłane do usługi Share Point, gdzie mogą być przeglądane prze pracowników HR. 