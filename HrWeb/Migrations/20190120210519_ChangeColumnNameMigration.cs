﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HrWeb.Migrations
{
    public partial class ChangeColumnNameMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PhotoBlobName",
                table: "JobApplications",
                newName: "CVBlobName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CVBlobName",
                table: "JobApplications",
                newName: "PhotoBlobName");
        }
    }
}
