﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HrWeb.Migrations
{
    public partial class UpdateJobApplicationsMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CvUrl",
                table: "JobApplications",
                newName: "PhotoBlobName");

            migrationBuilder.AlterColumn<string>(
                name: "JobTitle",
                table: "JobOffers",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "JobApplications",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "JobApplications");

            migrationBuilder.RenameColumn(
                name: "PhotoBlobName",
                table: "JobApplications",
                newName: "CvUrl");

            migrationBuilder.AlterColumn<string>(
                name: "JobTitle",
                table: "JobOffers",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
