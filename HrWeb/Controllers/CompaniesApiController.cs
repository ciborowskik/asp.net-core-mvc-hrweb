﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HrWeb.EntityFramework;
using HrWeb.Models;

namespace HrWeb.Controllers
{
    [Route("api/Companies")]
    [ApiController]
    public class CompaniesApiController : ControllerBase
    {
        private readonly DataContext _context;

        public CompaniesApiController(DataContext context)
        {
            _context = context;
        }

        // GET: api/JobOffer?search={search}&page={page}&pageSize={pageSize}
        /// <summary>
        /// Gets list of job offers. It is possible to filter offers by JobTitle by sending search parameter.
        /// Paging is also enabled.
        /// </summary>
        /// <remarks>Not providing search string will return all job offers</remarks>
        /// <param name="search">string to search job titles with</param>
        /// <param name="pageNo">page number</param>
        /// <param name="pageSize">page size</param>
        /// <returns>List of JobOffer objects and total number of pages</returns>
        [HttpGet]
        public ActionResult<IEnumerable<JobOffer>> GetCompanies([FromQuery] string search, [FromQuery] int pageNo = 1, [FromQuery] int pageSize = 5)
        {
            List<Company> companies = new List<Company>();
            if (string.IsNullOrEmpty(search))
                companies = _context.Companies.ToList();
            else
                companies = _context.Companies.Where(c => c.Name.ToLower().Contains(search.ToLower())).ToList();

            int totalRecord = companies.Count();
            int totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);

            if (pageNo > totalPage) pageNo = totalPage;

            var pagingViewModel = new PagingViewModel<Company>
            {
                TotalPages = totalPage
            };
            if (companies.Count() != 0)
                pagingViewModel.Rows = companies.OrderBy(c => c.Name).Skip((pageNo - 1) * pageSize).Take(pageSize);
            else
                pagingViewModel.Rows = new List<Company>();

            return Ok(pagingViewModel);
        }

        // GET api/JobOffer/{id}
        /// <summary>
        /// Returns Company with given id.
        /// </summary>
        /// <param name="id">id of Company</param>
        /// <returns>Company</returns>
        [HttpGet("{id}")]
        public IActionResult GetCompany([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var company = _context.Companies.Find(id);

            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }
    }
}