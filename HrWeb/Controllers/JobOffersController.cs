﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HrWeb.EntityFramework;
using HrWeb.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace HrWeb.Controllers
{
    public class JobOffersController : Controller
    {
        private readonly DataContext _context;
        private readonly string connectionString = "DefaultEndpointsProtocol=https;AccountName=hrwebblob;AccountKey=WDK33Vt5z7vtylMCptVfkloVe71Y6TecDzPkM1XeJGrKuOJCKWC6a+E5ka3afCwWKcJj5MN1aLkB07mMrgSQaQ==;EndpointSuffix=core.windows.net";


        public JobOffersController(DataContext context)
        {
            _context = context;
        }

        // GET: JobOffers
        public async Task<IActionResult> Index()
        {
            return View();
        }

        // GET: JobOffers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobOffer = await _context.JobOffers
                .Include(j => j.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (jobOffer == null)
            {
                return NotFound();
            }

            return View(jobOffer);
        }

        // GET: JobOffers/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
            return View();
        }

        // POST: JobOffers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("JobTitle,CompanyId,SalaryFrom,SalaryTo,Location,Description,ValidUntil")] JobOffer jobOffer)
        {
            if(jobOffer.SalaryFrom>jobOffer.SalaryTo)
                ModelState.AddModelError(string.Empty, "Salary To must not be less than Salary From.");

            if (jobOffer.ValidUntil <= DateTime.Today.Date)
                ModelState.AddModelError(string.Empty, "Valid Until date must not be past.");

            if (ModelState.IsValid)
            {
                jobOffer.Created = DateTime.Now.Date;
                _context.Add(jobOffer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", jobOffer.CompanyId);
            return View(jobOffer);
        }

        // GET: JobOffers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobOffer = await _context.JobOffers.FindAsync(id);
            if (jobOffer == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", jobOffer.CompanyId);
            return View(jobOffer);
        }

        // POST: JobOffers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,JobTitle,CompanyId,SalaryFrom,SalaryTo,Created,Location,Description,ValidUntil")] JobOffer jobOffer)
        {
            if (id != jobOffer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(jobOffer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JobOfferExists(jobOffer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", jobOffer.CompanyId);
            return View(jobOffer);
        }


        // POST: JobOffers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobOffer = await _context.JobOffers
                .Include(j => j.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (jobOffer == null)
            {
                return NotFound();
            }

            _context.JobOffers.Remove(jobOffer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: JobApplications/Create
        public IActionResult Apply(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            JobOffer jobOffer = _context.JobOffers.Include(j => j.Company).FirstOrDefault(j => j.Id == id);
            if (jobOffer == null)
            {
                return NotFound();
            }

            JobApplicationCreateView jobApplication = new JobApplicationCreateView();
            jobApplication.JobOfferId = id.Value;
            jobApplication.JobOffer = jobOffer;

            return View(jobApplication);
        }

        // POST: JobOffers/Apply
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Apply([Bind("JobOfferId,FirstName,LastName,PhoneNumber,EmailAddress,Description,ContactAgreement,CV")] JobApplicationCreateView jobApplication)
        {
            JobOffer jobOffer = _context.JobOffers.Include(j => j.Company).FirstOrDefault(j => j.Id == jobApplication.JobOfferId);
            if (jobOffer == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                jobApplication.Created = DateTime.Now.Date;

                if (jobApplication.CV != null)
                {
                    int length = (int)jobApplication.CV.Length;
                    byte[] file = new byte[length];
                    jobApplication.CV.OpenReadStream().ReadAsync(file, 0, length);

                    int newId = _context.JobApplications.Max(a => a.Id) + 1;
                    string blobReference = $"{jobOffer.Company.Name}_{jobOffer.JobTitle}#{newId}{Path.GetExtension(jobApplication.CV.FileName)}";

                    CloudStorageAccount storageAccount = null;
                    if (CloudStorageAccount.TryParse(connectionString, out storageAccount))
                    {
                        CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                        CloudBlobContainer container = cloudBlobClient.GetContainerReference("cvs");
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobReference);
                        jobApplication.CVBlobName = blobReference;

                        await blockBlob.UploadFromByteArrayAsync(file, 0, file.Length);
                    }
                }

                _context.Add(jobApplication);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Details), new { id = jobApplication.JobOfferId });
            }

            jobApplication.JobOffer = jobOffer;

            return View(jobApplication);
        }

        public async Task<IActionResult> ApplicationDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobApplication = await _context.JobApplications
                .Include(j => j.JobOffer)
                .ThenInclude(o => o.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (jobApplication == null)
            {
                return NotFound();
            }

            return View(jobApplication);
        }

        // GET: JobApplications/Edit/5
        public async Task<IActionResult> ApplicationEdit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobApplication = await _context.JobApplications.FindAsync(id);
            if (jobApplication == null)
            {
                return NotFound();
            }

            return View(jobApplication);
        }

        // POST: JobApplications/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApplicationEdit(int id, [Bind("Id,JobOfferId,FirstName,LastName,PhoneNumber,EmailAddress,Description,ContactAgreement,Created,CVBlobName")] JobApplication jobApplication)
        {
            if (id != jobApplication.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(jobApplication);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JobApplicationExists(jobApplication.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Details), new { id = jobApplication.JobOfferId });
            }

            return View(jobApplication);
        }

        [HttpPost, ActionName("ApplicationDelete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApplicationDeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobApplication = await _context.JobApplications.FindAsync(id);
            _context.JobApplications.Remove(jobApplication);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new { id = jobApplication.JobOfferId });
        }

        private bool JobApplicationExists(int id)
        {
            return _context.JobApplications.Any(e => e.Id == id);
        }

        private bool JobOfferExists(int id)
        {
            return _context.JobOffers.Any(e => e.Id == id);
        }
    }
}
