﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HrWeb.EntityFramework;
using HrWeb.Models;

namespace HrWeb.Controllers
{
    [Route("api/JobOffers")]
    [ApiController]
    public class JobOffersApiController : ControllerBase
    {
        private readonly DataContext _context;

        public JobOffersApiController(DataContext context)
        {
            _context = context;
        }

        // GET: api/JobOffers?search={search}&page={page}&pageSize={pageSize}
        /// <summary>
        /// Gets list of job offers. It is possible to filter offers by JobTitle by sending search parameter.
        /// Paging is also enabled.
        /// </summary>
        /// <remarks>Not providing search string will return all job offers</remarks>
        /// <param name="search">string to search job titles with</param>
        /// <param name="pageNo">page number</param>
        /// <param name="pageSize">page size</param>
        /// <returns>List of JobOffer objects and total number of pages</returns>
        [HttpGet]
        public ActionResult<PagingViewModel<JobOffer>> GetJobOffers([FromQuery] string search, [FromQuery] int pageNo = 1, [FromQuery] int pageSize = 5)
        {
            List<JobOffer> offers = new List<JobOffer>();
            if (string.IsNullOrEmpty(search))
                offers = _context.JobOffers.Include(j => j.Company).ToList();
            else
                offers = _context.JobOffers.Where(o => o.JobTitle.ToLower().Contains(search.ToLower())).Include(j => j.Company).ToList();

            int totalRecord = offers.Count();
            int totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);

            if (pageNo > totalPage) pageNo = totalPage;

            var pagingViewModel = new PagingViewModel<JobOffer>
            {
                TotalPages = totalPage
            };
            if (offers.Count() != 0)
                pagingViewModel.Rows = offers.OrderBy(o => o.JobTitle).Skip((pageNo - 1) * pageSize).Take(pageSize);
            else
                pagingViewModel.Rows = new List<JobOffer>();

            return Ok(pagingViewModel);
        }

        // GET api/JobOffers/{id}
        /// <summary>
        /// Returns JobOffer with given id.
        /// </summary>
        /// <param name="id">id of JobOffer</param>
        /// <returns>JobOffer</returns>
        [HttpGet("{id}")]
        public IActionResult GetJobOffer([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var jobOffer = _context.JobOffers.Find(id);

            if (jobOffer == null)
            {
                return NotFound();
            }

            return Ok(jobOffer);
        }


        // GET api/JobOffers/{id}/applications?page={page}&pageSize={pageSize}
        /// <summary>
        /// Gets all JobApplications for job offer with given id.
        /// </summary>
        /// <param name="id">id of JobOffer</param>
        /// <param name="pageNo">page number</param>
        /// <param name="pageSize">page size</param>
        /// <returns>List of JobApplication objects</returns>
        [HttpGet("{id}/applications")]
        public ActionResult<PagingViewModel<JobApplication>> GetApplicationsForJobOffer([FromRoute] int id, [FromQuery] int pageNo = 1, [FromQuery] int pageSize = 5)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var applications = _context.JobApplications.Where(a => a.JobOfferId == id);

            int totalRecord = applications.Count();
            int totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);

            if (pageNo > totalPage) pageNo = totalPage;

            var pagingViewModel = new PagingViewModel<JobApplication>
            {
                TotalPages = totalPage
            };
            if (applications.Count() != 0)
                pagingViewModel.Rows = applications.OrderBy(a => a.LastName).Skip((pageNo - 1) * pageSize).Take(pageSize);
            else
                pagingViewModel.Rows = new List<JobApplication>();

            return Ok(pagingViewModel);
        }
    }
}