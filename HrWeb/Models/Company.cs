﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HrWeb.Models
{
    public class Company
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [JsonIgnore]
        public List<JobOffer> JobOffers { get; set; } = new List<JobOffer>();
    }
}
