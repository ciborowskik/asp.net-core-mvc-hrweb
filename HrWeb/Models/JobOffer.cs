﻿using HrWeb.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HrWeb.Models
{
    public class JobOffer
    {
        public int Id { get; set; }

        [Display(Name = "Company")]
        public virtual int CompanyId { get; set; }

        public virtual Company Company { get; set; }

        [Required]
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0}")]
        [Display(Name = "Salary From")]
        [Range(minimum:0, maximum:double.MaxValue, ErrorMessage = "Salary have to be positive value.")]
        public decimal? SalaryFrom { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0}")]
        [Display(Name = "Salary To")]
        [Range(minimum: 0, maximum: double.MaxValue, ErrorMessage = "Salary have to be positive value.")]
        public decimal? SalaryTo { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter))]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Created")]
        public DateTime Created { get; set; }

        [Display(Name = "Location")]
        public string Location { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString ="{0:yyyy-MM-dd}")]
        [Display(Name ="Valid Until")]
        public DateTime? ValidUntil { get; set; }

        [JsonIgnore]
        public List<JobApplication> JobApplications { get; set; } = new List<JobApplication>();
    }
}
