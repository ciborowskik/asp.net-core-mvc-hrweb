﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using HrWeb.Helpers;
using Microsoft.AspNetCore.Http;

namespace HrWeb.Models
{
    public class JobApplication
    {
        public int Id { get; set; }

        public virtual int JobOfferId { get; set; }

        public virtual JobOffer JobOffer { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})$", ErrorMessage = "Phone number format: xxx-xxx-xxx")]
        [Display(Name = "Contact Phone")]
        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Contact Email")]
        public string EmailAddress { get; set; }

        [Display(Name = "Additional Information")]
        public string Description { get; set; }

        [Required]
        [MustBeTrue(ErrorMessage = "You have to aggre for contact")]
        [Display(Name = "Contact agreement")]
        public bool ContactAgreement { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Created")]
        public DateTime Created { get; set; }

        public string CVBlobName { get; set; }
    }
}
