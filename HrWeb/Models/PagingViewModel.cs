﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrWeb.Models
{
    public class PagingViewModel<T>
    {
        public IEnumerable<T> Rows { get; set; }
        public int TotalPages { get; set; }
    }
}
