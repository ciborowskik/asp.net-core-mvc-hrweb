﻿using HrWeb.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HrWeb.Models
{
    public class JobApplicationCreateView : JobApplication
    {
        [JPGExtension(ErrorMessage = "Please select pdf file")]
        [MaxSize(MaxSize = 1024 * 1024, ErrorMessage = "Max size is 1MB")]
        public IFormFile CV { get; set; }
    }
}
