﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace HrWeb.Helpers
{
    public class MaxSizeAttribute : ValidationAttribute
    {
        public long MaxSize { get; set; } = 1024 * 1024;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return null;

            if (value != null && value is IFormFile && ((IFormFile)value).Length <= MaxSize)
                return null;

            else
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
        }
    }
}
